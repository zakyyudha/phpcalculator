<?php


namespace Jakmall\Recruitment\Calculator\History;


use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryFileStorage implements CommandHistoryManagerInterface
{
    const FILENAME = __DIR__ . '/../../storage/database.json';

    public function findAll(array $commands): array
    {
        $data = $this->readFileData()
            ->map(function ($item, $key) {
                return collect($item)
                    ->only(['command', 'description', 'result', 'output', 'time'])
                    ->all();
            })->values();

        if (!empty($commands)) {
            $data = $data->whereIn('command', array_map('ucfirst', $commands))->values();
        }

        return $data->map(function ($item, $key){
                return array_merge(['no' => $key + 1], $item);
            })->all();
    }

    public function log($command): bool
    {
        // TODO: Implement log() method.
    }

    public function clearAll(): bool
    {
        // TODO: Implement clearAll() method.
    }

    protected function readFileData()
    {
        if (!file_exists(self::FILENAME)) {
            $currentData = collect();
        } else {
            $currentData = collect(json_decode(
                file_get_contents(self::FILENAME)
            ));
        }
        return $currentData;
    }
}
