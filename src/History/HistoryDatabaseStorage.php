<?php


namespace Jakmall\Recruitment\Calculator\History;


use Illuminate\Database\Eloquent\Builder;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\History\Models\Database;
use Jakmall\Recruitment\Calculator\History\Models\History;

class HistoryDatabaseStorage implements CommandHistoryManagerInterface
{
    const FILENAME = __DIR__ . '/../../storage/database.json';

    public function __construct()
    {
        new Database();
    }

    public function findAll(array $commands): array
    {
        $data = History::select(['command', 'description', 'result', 'output', 'time'])
            ->where(function (Builder $query) use ($commands) {
                if (!empty($commands)) {
                    $query->whereIn('command', array_map('ucfirst', $commands));
                }
                return $query;
            })
            ->orderBy('time', 'ASC')
            ->get();

        $pushNo = $data->map(function (History $item, $key) {
            return array_merge(['no' => $key + 1], $item->toArray());
        });

        return $pushNo->toArray();
    }

    public function log($command): bool
    {
        return (bool)$this->logToDB($command);
    }

    public function clearAll(): bool
    {
        return (bool)(History::truncate() &&
            file_put_contents(self::FILENAME, collect()->toJson())
        );
    }

    protected function logToDB(array $commands)
    {
        $db = History::create($commands);
        $file = $this->logToFile($db);

        return (bool)($db && $file);
    }

    protected function logToFile(History $model)
    {
        $currentData = $this->readFileData();
        $save = file_put_contents(self::FILENAME,
            $currentData->push($model)
                ->toJson()
        );

        return (bool)$save;
    }

    protected function readFileData()
    {
        if (!file_exists(self::FILENAME)) {
            $currentData = collect();
        } else {
            $currentData = collect(json_decode(
                file_get_contents(self::FILENAME)
            ));
        }
        return $currentData;
    }
}
