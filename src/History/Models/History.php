<?php


namespace Jakmall\Recruitment\Calculator\History\Models;


use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    public const CREATED_AT = 'time';

    protected $table = 'histories';
    protected $fillable = [
        'command',
        'description',
        'result',
        'output',
        'time'
    ];
}
