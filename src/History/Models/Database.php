<?php


namespace Jakmall\Recruitment\Calculator\History\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Database
{
    public function __construct()
    {
        $capsule = new Capsule;
        $capsule->addConnection([
            'driver' => $_ENV['DB_CONNECTION'],
            'host' => $_ENV['DB_HOST'],
            'port' => $_ENV['PORT'],
            'database' => $_ENV['DB_DATABASE'],
            'username' => $_ENV['DB_USERNAME'],
            'password' => $_ENV['DB_PASSWORD'],
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ]);

        $capsule->setAsGlobal();

        // Setup the Eloquent ORM…
        $capsule->bootEloquent();

        $this->createTable();
    }

    protected function createTable()
    {
        if(!Capsule::schema()->hasTable('histories')){
            Capsule::schema()->create('histories', function(Blueprint $table){
                $table->increments('id');
                $table->string('command');
                $table->string('description');
                $table->string('result');
                $table->string('output');
                $table->timestamp('time');
                $table->timestamp('updated_at');
            });
        }
    }
}
