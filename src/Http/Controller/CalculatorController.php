<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class CalculatorController
{
    protected $action, $input, $operator, $history;

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;
    }

    public function calculate($action, Request $request)
    {
        $this->action = strtolower($action);
        $this->input = array_filter((array) $request->post('input'));

        if($response = $this->validate()){
            return $response;
        }

        $result = $this->calculateAll($this->input);
        $operation = $this->generateCalcululationOperation($this->input);

        $this->history->log(
            $this->log($operation, $result)
        );

        return JsonResponse::create(
            $this->response($operation, $result)
        );
    }

    protected function log($description, $result) : array
    {
        return [
            'command' => ucfirst($this->action),
            'description' => $description,
            'result' => $result,
            'output' => sprintf('%s = %s', $description, $result)
        ];
    }

    protected function response($operation, $result) : array
    {
        return [
            'command' => $this->action,
            'operation' => $operation,
            'result' => $result
        ];
    }

    protected function validate()
    {
        if(!in_array($this->action, ['add', 'subtract', 'multiply', 'divide', 'pow'])) {
            return JsonResponse::create([
                'error' => [
                    'code' => 422,
                    'message' => sprintf('Unknown action request {%s}', $this->action)
                ]
            ], 422);
        }

        if(empty($this->input)){
            return JsonResponse::create([
                'error' => [
                    'code' => 422,
                    'message' => 'Input parameter required'
                ]
            ], 422);
        }

        return false;
    }

    protected function generateCalcululationOperation(array $numbers) : string
    {
        $operator = $this->operator;
        $glue = sprintf(' %s ', $operator);

        return implode($glue, $numbers);
    }

    protected function calculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->action($this->calculateAll($numbers), $number);
    }

    protected function action(int $number1,int $number2) : int
    {
        switch ($this->action) {
            case 'add' :
                $this->operator = '+';
                return $number1 + $number2;
                break;

            case 'subtract' :
                $this->operator = '-';
                return $number1 - $number2;
                break;

            case 'multiply' :
                $this->operator = '*';
                return $number1 * $number2;
                break;

            case 'divide' :
                $this->operator = '/';
                return $number1 / $number2;
                break;

            case 'pow' :
                $this->operator = '^';
                return pow($number1, $number2);
                break;
        }
    }
}
