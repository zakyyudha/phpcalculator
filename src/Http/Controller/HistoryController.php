<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\History\Models\Database;
use Jakmall\Recruitment\Calculator\History\Models\History;

class HistoryController
{
    protected $history;
    const FILENAME = __DIR__ . '/../../../storage/database.json';

    public function __construct()
    {
        new Database();
    }

    public function index()
    {
        $response = History::all()->map(function (History $model){
            return $this->mapResult($model);
        });

        return JsonResponse::create(
            $response->toArray()
        );
    }

    public function show($id)
    {
        $idNumber = intval(str_replace('id', '', $id));
        $result = $this->findBy($idNumber);

        if($result instanceof JsonResponse){
            return $result;
        }

        return JsonResponse::create(
            $this->mapResult($result)
        );
    }

    public function remove($id)
    {
        $idNumber = intval(str_replace('id', '', $id));
        $resultOnDb = $this->findBy($idNumber);

        if($resultOnDb instanceof JsonResponse){
            return $resultOnDb;
        }

        $resultOnDb->delete();
        $this->deleteFileData($idNumber);

        return JsonResponse::create('', 204);
    }

    protected function deleteFileData($id)
    {
        if (!file_exists(self::FILENAME)) {
            $currentData = collect();
        } else {
            $currentData = collect(json_decode(
                file_get_contents(self::FILENAME)
            ));
        }

        $filtered = $currentData->filter(function($item, $key) use ($id){
            $item = (array) $item;
            return $item['id'] != $id;
        });

        $save = file_put_contents(self::FILENAME,
            $filtered->toJson()
        );

        return (bool)$save;
    }



    /**
     * @param $id
     * @return JsonResponse|History
     */
    protected function findBy($id)
    {
        $result = History::find($id);
        if(!$result){
            return JsonResponse::create([
                'error' => [
                    'code' => 404,
                    'message' => sprintf('No result for requested id {%s}', $id)
                ]
            ], 404);
        }
        return $result;
    }

    protected function mapResult(History $model) : array
    {
        $item = $model->toArray();

        $result['id'] = 'id'.$item['id'];
        $result['command'] = $item['command'];
        $result['operation'] = $item['description'];
        $result['input'] = array_map('intval', preg_split("/[+^\/-]/", $item['description']));
        $result['result'] = $item['result'];
        $result['time'] = $item['time'];

        return $result;
    }
}
