<?php


namespace Jakmall\Recruitment\Calculator\Commands;


use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\HistoryFileStorage;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryListCommand extends Command
{
    protected $signature = 'history:list
            {commands?* : Filter the history by commands}
            {--D|driver=database : Driver for storage connection}';

    protected $description = 'Show calculator history';

    public function handle()
    {
        $commands = $this->argument('commands');
        $driver = $this->option('driver');

        if(!in_array($driver, ['file', 'database'])){
            $this->error('Unknown storage driver');
            exit(0);
        }

        $history = $this->getLaravel()
            ->make(CommandHistoryManagerInterface::class);

        if($driver == 'file') {
            $this->getLaravel()->bind(
                CommandHistoryManagerInterface::class,
                HistoryFileStorage::class
            );

            $history = $this->getLaravel()
                ->make(CommandHistoryManagerInterface::class);
        }

        $headers = ['No', 'Command', 'Description', 'Result', 'Output', 'Time'];
        $data = $history->findAll($commands);

        if(empty($data)){
            $this->info('History is empty.');
            exit(0);
        }

        $this->table($headers, $data);
    }
}
