<?php


namespace Jakmall\Recruitment\Calculator\Commands;


use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

/**
 * Class PowCommand
 * @package Jakmall\Recruitment\Calculator\Commands
 */
class PowCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var string
     */
    protected $name;

    /**
     * PowCommand constructor.
     */
    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {base : The base number} {exp : The exponent number}',
            $commandVerb
        );

        $this->description = 'Exponent the given number';
        $this->name = 'Pow';

        parent::__construct();
    }

    /**
     * @return string
     */
    protected function getCommandVerb(): string
    {
        return 'pow';
    }

    public function handle(CommandHistoryManagerInterface $history): void
    {
        $base = $this->argument('base');
        $exp = $this->argument('exp');
        $description = $this->generateCalculationDescription($base, $exp);
        $result = $this->calculate($base, $exp);

        $this->comment(sprintf('%s = %s', $description, $result));

        $history->log([
            'command' => 'Pow',
            'description' => $description,
            'result' => $result,
            'output' => sprintf('%s = %s', $description, $result)
        ]);
    }

    /**
     * @param string $base
     * @param string $exp
     * @return string
     */
    protected function generateCalculationDescription(string $base, string $exp): string
    {
        $operator = $this->getOperator();
        $glue = sprintf(' %s ', $operator);

        return $base . $glue . $exp;
    }

    /**
     * @return string
     */
    protected function getOperator(): string
    {
        return '^';
    }

    /**
     * @param $base
     * @param $exp
     * @return float|int
     */
    protected function calculate($base, $exp)
    {
        return pow($base, $exp);
    }
}
